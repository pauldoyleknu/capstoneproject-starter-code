#!/usr/bin/python

# raw_input in Python 2 reads in a string
student = raw_input('Hello KNU student, please Enter your name: ')

# you can now print out the string
print 'Hello ', student


# We can convert the string we read into a number

age = int(raw_input('Please Enter your age: '))

# We can now use the number to text for conditions and reply

if age > 20:
	print 'you must be a final year student! ', student
else:
	print 'Still a few years to go to finish! ', student

studentnum = raw_input('Please Enter your student number: ')
familyname = raw_input('Please Enter your family name ')

print '----------------------'
print '-     Student Record  '
print '-Name:        ',student
print '-Family Name: ',familyname
print '-Age:         ',age
print '-Number:      ',studentnum

