#!/usr/bin/python
import sys
# Author: Paul Doyle

#Read in the string to test
mystring=sys.argv[1]

#Start with the assumption that it is correct.
palindrometest = True

#Find out the length of the palindrome
stringlength = len(mystring)

# we test to see if the string has an even number of chacters
# If there is no remainder the the value is 0

if stringlength % 2 ==0:
	# The string is even in lenght, so we will test exactly half of the 
  	# string against the second half of the string 
	halfway = stringlength/2;
else:
	# The string is odd, so we don't have to test the middle value, so 
	# we will test the first half of the string, not include the middle value
	halfway = (stringlength-1)/2;
	
# iterate through the first half of the string, comparing it to the second half of the string
for i in range(halfway):
	# we walk forward through first half of the string and reverse through the second half
      	# comparing the values whcih must be the same.
	if mystring[i] <> mystring[stringlength-1-i]:
		palindrometest = False  # if they are not the same, this is not a palindrome


# If none of the tests were false then we have a palindrome
if palindrometest == True:
	print 'This is a palindrome!'
else:
	print 'This is not a palindrome'
