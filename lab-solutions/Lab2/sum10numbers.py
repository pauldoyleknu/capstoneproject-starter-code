#!/usr/bin/python

n = 10
sum1 = 0

# Loop through the numbers counting down from 10 to 1 
# and add them to the variable sum1
# the counter is n which is reduced each time we run the loop
while(n > 0):
    sum1=sum1+n
    n=n-1
print("The sum of first n natural numbers is",sum1)
