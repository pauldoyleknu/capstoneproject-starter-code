#!/usr/bin/python
import sys

balance=int(sys.argv[1])

years = 3
sum1 = 0

# Loop through the numbers counting down from 10 to 1 
# and add them to the variable sum1
# the counter is n which is reduced each time we run the loop
while(years > 0):
    balance=balance * 1.05
    years=years-1
print("The final balance is = ",balance)
