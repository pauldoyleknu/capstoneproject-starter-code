#This script deletes a queues 
#
# Author - Paul Doyle June 2017
#
#
import boto3
import json
import argparse


# Get the keys from a aws configuration file embedded in the virtual machine
# Change the default region used to Seoul

# Parse the argumne from the commandline
parser = argparse.ArgumentParser()
parser.add_argument("qname")
args = parser.parse_args()

def convert_to_json(urllist):
	all = []
	each = {}
	each['url'] = urllist
	all.append(each)
	print json.dumps(all)
	return



sqs  = boto3.client('sqs',region_name="ap-northeast-2")

try:
    response = sqs.get_queue_url(QueueName=args.qname)
    sqs.delete_queue(QueueUrl=response['QueueUrl'])
    print "Deleted Queue"
except:
    print "Failed to Delete Queue"
