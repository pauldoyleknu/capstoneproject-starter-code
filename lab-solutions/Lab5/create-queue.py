#This script creates a queues 
#
# Author - Paul Doyle June 2017
#
#
import boto3
import json
import argparse


# Get the keys from a aws configuration file embedded in the virtual machine
# Change the default region used to Seoul

# Parse the argumne from the commandline
parser = argparse.ArgumentParser()
parser.add_argument("qname")
args = parser.parse_args()

def convert_to_json(urllist):
	all = []
	each = {}
	each['url'] = urllist
	all.append(each)
	print json.dumps(all)
	return



sqs  = boto3.client('sqs',region_name="ap-northeast-2")

# Get a list of the queues that exists and then print the list out
queue = sqs.create_queue(QueueName=args.qname)
url = queue['QueueUrl']

# Convert the output to JSON format 
convert_to_json(url)
