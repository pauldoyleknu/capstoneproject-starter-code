# This script lists all queues in defaul region
#
# Author - Paul Doyle June 2017
#
#
import boto3
import json
# Get the keys from a aws configuration file embedded in the virtual machine
# Change the default region used to Seoul


def convert_to_json(urllist):
	all = []
	each = {}
	each['url'] = urllist
	all.append(each)
	print json.dumps(all)
	return



sqs  = boto3.client('sqs',region_name="ap-northeast-2")

# Get a list of the queues that exists and then print the list out
response = sqs.list_queues()

# Convert the list to JSON format
convert_to_json(response['QueueUrls'])
