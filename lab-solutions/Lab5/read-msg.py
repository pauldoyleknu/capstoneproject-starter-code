#This script writes a message to a queues 
#
# Author - Paul Doyle June 2017
#
#
import boto3
import json
import argparse


# Get the keys from a aws configuration file embedded in the virtual machine
# Change the default region used to Seoul

# Parse the argument from the commandline
parser = argparse.ArgumentParser()
parser.add_argument("qname")
parser.add_argument("message")
args = parser.parse_args()

def convert_to_json(urllist):
	all = []
	each = {}
	each['message_data'] = urllist
	all.append(each)
	print json.dumps(all)
	return


# create tbe clist resource
sqs  = boto3.client('sqs',region_name="ap-northeast-2")

# Get the queue url

response = sqs.get_queue_url(QueueName=args.qname)  
url = response['QueueUrl']

# Create a new message and check the message response
response = sqs.receive_message(QueueUrl=url,MaxNumberOfMessages=1)
message = response['Messages'][0]
# print message['Body']

# Create a list with the message information in it
msgdata = [message,url]

# Convert the output to JSON format 
convert_to_json(msgdata)
