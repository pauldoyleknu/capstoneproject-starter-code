#Author: Paul Doyle
# Date: June 2017

from flask import Flask, Response, request
from json import dumps

app = Flask(__name__)

@app.route('/')
def index():
  return """

Available API endpoints:

POST /interest			addition of 2 numbers 
POST /pow			find the power of a number
POST /metric			convert from meters to inches
POST /max			find max number of two numbers
POST /min			find the power of a number

GET /functions		return a list of supported functions
"""

#
#
# This is the default location for the web server
# When users access the website, the front page will explain the 
# REST APIs that are accessible. 
# The first two examples have been completed, you will need to 
# fill out the code to support the remaining API calls
#
#

app = Flask(__name__)

#
# Example:
#
# curl -s -X GET -H 'Accept: application/json' https://web.134vm.nightsky.ie/functions
# {"operators": ["interest", "pow", "metric", "max","min"]}
#
@app.route('/functions', methods=['GET'])
def operators():
  resp = dumps({ 
    "functions":["interest","pow","metric","max","min"]
  })
  return Response(response=resp, mimetype="application/json")

#
# Example:
#
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/interest -d '{"principle":100,"years":20,"ratepercent":5}'
# {"result": 30}
#
@app.route('/interest', methods=['POST'])
def interest():
  args = request.get_json()
  balance = int(args['principle'])
  rate= float(args['ratepercent'])/100
  term = int(args['years'])

  print args
  try:
    while(term > 0):
	balance = balance * (1 + rate)
	term = term -1

  except Exception:
    pass

  resp = dumps({
    "result": balance
  })
  
  return Response(response=resp, mimetype="application/json")


#
# Example:
#   
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/min -d '{"n1":100,"n2":20}'
# {"result": 20}
#
@app.route('/min', methods=['POST'])
def min():
  args = request.get_json()
  n1 = int(args['n1'])
  n2 = int(args['n2'])

  if n1 < n2:
	result = n1
  else:
        result = n2
  resp = dumps({
    "result": result
  })

  return Response(response=resp, mimetype="application/json")

#
# Example:
#   
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/max -d '{"n1":100,"n2":20}'
# {"result": 100}
#
@app.route('/max', methods=['POST'])
def max():
  args = request.get_json()
  number1 = int(args['n1'])
  number2 = int(args['n2'])
  
  if number1 > number2:
	result = number1
  else:
        result = number2
  resp = dumps({
    "result": result
  }) 
 
  return Response(response=resp, mimetype="application/json")

#
# Example:
#   
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/pow -d '{"n1":10,"n2":2}'
# {"result": 100}
#
@app.route('/pow', methods=['POST'])
def pow():
  args = request.get_json()
  number1 = int(args['n1'])
  power = int(args['n2'])
  
  while (power > 1):
     number1 = number1 * number1
     power = power-1
 
  resp = dumps({
    "result": number1
  }) 
 
  return Response(response=resp, mimetype="application/json")

#
# Example:
#   
# curl -s -X POST -H 'Accept: application/json' -H 'Content-type: application/json' https://web.134vm.nightsky.ie/metric -d '{"n1":100}'
# {"result": 100}
#
@app.route('/metric', methods=['POST'])
def metric():
  args = request.get_json()
  n1 = int(args['n1'])

  result = n1 / .03048
  resp = dumps({
    "result": result
  })

  return Response(response=resp, mimetype="application/json")




if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=5000)
