from flask import Flask, request
app = Flask(__name__)

template = '''
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style>body { margin: 20px; } </style>
</head>
<body>
  <form action="/calculate" method="post" class="form-inline">
    <input type="number" class="form-control" name="n1" value="%d" autofocus>
    <select name="operator" class="form-control">
      <option>+</option>
      <option>-</option>
      <option>x</option>
      <option>max</option>
      <option>sum</option>
      <option>&#247;</option>
    </select>
    <input type="number" class="form-control" name="n2" value="%d">
    <button type="submit" class="btn btn-primary">Calculate</button>
    &rarr;
    <input type="number" class="form-control" value="%d" readonly>
  </form>
</body>
</html>
'''

@app.route('/')
def index():
  return template % (0, 0, 0)

@app.route('/calculate', methods=['POST'])
def calculate():
  args = request.form
  result = 0
  sum1 = 0
  highnum = int(args['n2'])
  lownum = int(args['n1'])
  try:
    if args['operator'] == u'+':
      result = int(args['n1']) + int(args['n2'])
    elif args['operator'] == u'-':
      result = int(args['n1']) - int(args['n2'])
    elif args['operator'] == u'x':
      result = int(args['n1']) * int(args['n2'])
    elif args['operator'] == u'sum':
	while (highnum >= lownum):
		sum1=sum1+highnum
		highnum=highnum-1
	result = sum1
    elif args['operator'] == u'max':
	if int(args['n1']) > int(args['n2']):	
      		result = int(args['n1']) 
	else:
      		result = int(args['n2']) 
    else:
      result = int(args['n1']) / int(args['n2'])
  except Exception:
    pass

  return template % (int(args['n1']), int(args['n2']), result)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=5000)
