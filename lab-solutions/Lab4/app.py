from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World! - This is an update"

@app.route('/cct')
def cct():
    return "Welcome to Cloud Computing Technologies"
@app.route('/knu')
def knu():
    return "Welcome to KNU"

@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User is %s' % username

@app.route('/square/<int:inputnum>')
def show_post(inputnum):
    # show the post with the given id, the id is an integer
    return 'Square is %d' % (inputnum * inputnum) 


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
